import React, { Component } from 'react'
 
export default class NavControls extends Component {
  constructor(props) {
    super(props)

    this.state = {activeMenu: null}
    this.selectMenu = this.selectMenu.bind(this)
    this.beforeRouting = this.beforeRouting.bind(this)
  }

  selectMenu(menuId) {
    this.setState({activeMenu: menuId})
  }

  beforeRouting(callback) {
    this.selectMenu(null)

    if (this.props.beforeRouting) {
      this.props.beforeRouting(callback)
    }
    else {
      callback()
    }
  }

  render() {
    const navControls = React.Children.map(this.props.children, (child) =>
      React.cloneElement(child, {
        activeMenu: this.state.activeMenu,
        selectMenu: this.selectMenu,
        beforeRouting: this.beforeRouting
      })
    )

    return (
      <ul className="nav-controls">{navControls}</ul>
    )
  }
}
