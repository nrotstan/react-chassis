import React from 'react'

export default Copyright = function(props) {
  return (
    <div className="copyright">
      &copy; {(new Date()).getFullYear()} {props.companyName}
    </div>
  )
}
