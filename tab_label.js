import React, { Component } from 'react'
import classNames from 'classnames'

export default class TabLabel extends Component {
  render() {
    const isActive = this.props.activeTab === this.props.tabId

    return (
      <li key={this.props.tabId}
          className={classNames('tab', {active: isActive})}
          onClick={() => this.props.selectTab(this.props.tabId)}>
        <div className="tab-label">{this.props.children}</div>
      </li>
    )
  }
}
