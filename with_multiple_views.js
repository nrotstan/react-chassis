import React, { Component } from 'react'

export default function WithMultipleViews(WrappedComponent, defaultViewType) {
  return class extends Component {
    constructor(props) {
      super(props)

      this.state = {
        viewType: null,
      }

      this.changeViewType = this.changeViewType.bind(this)
    }

    changeViewType(newType) {
      this.setState({viewType: newType})
    }

    render() {
      const viewType = this.state.viewType || defaultViewType || 'tile'
      return <WrappedComponent {...this.props}
                               viewType={viewType}
                               changeViewType={this.changeViewType} />
    }
  }
}
