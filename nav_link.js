import React, { Component } from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'

import RouteLink from './route_link'
 
export default class NavLink extends Component {
  render() {
    return (
      <li className="nav-control nav-link" key={this.props.menuId}>
        <RouteLink {..._.omit(this.props, 'children')}>
          <span className="control-name">{this.props.name}</span>
        </RouteLink>
      </li>
    )
  }
}

NavLink.propTypes = {
  menuId: PropTypes.string.isRequired,
}
