import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import _ from 'lodash'

import Dropdown from './dropdown'

export default class SplitButtonDropdown extends Component {
  render() {
    let dropdownMenu = null
    if (_.compact(this.props.children).length > 0) {
      dropdownMenu = <Dropdown {...this.props} />
    }

    return (
      <div className={classNames('split-button-dropdown',
                                 {'without-dropdown': dropdownMenu === null},
                                 this.props.className)}>
        <div className="split-button-dropdown__wrapper">
          {this.props.button}
          {dropdownMenu}
        </div>
      </div>
    )
  }
}
