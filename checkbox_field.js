import React, { Component } from 'react'
import PropTypes from 'prop-types'
 
export default class CheckboxField extends Component { 
  constructor(props) {
    super(props)
    this.state = {fieldValue: this.props.fieldValue || false}

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    this.props.valueSet(this.props.name, !this.state.fieldValue)
    this.setState({fieldValue: !this.state.fieldValue})
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.hybridManagement) {
      if (typeof nextProps.fieldValue !== 'undefined' &&
          nextProps.fieldValue !== this.state.fieldValue) {
        this.setState({fieldValue: nextProps.fieldValue})
      }
    }
  }

  render() {

    return (
      <div className="form-group checkbox-input">
        <label>
          <input type="checkbox"
                name={this.props.name}
                checked={this.state.fieldValue}
                onChange={this.handleChange} />
          {this.props.label}
        </label>
      </div>
    )
  }
}

CheckboxField.propTypes = {
  valueSet: PropTypes.func,
  name: PropTypes.string
}
