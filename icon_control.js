import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class IconControl extends Component {
  render() {
    const classNames = `icon-control ${this.props.className || ''}`
    const cleanProps = _.omit(this.props, 'className', 'icon')

    let children = null

    if (this.props.children) {
      children = this.props.children
    }
    else if (this.props.icon) {
      children = <i className={this.props.icon}></i>
    }

    return (
      <div className={classNames} {...cleanProps}>
        {children}
      </div>
    )
  }
}

IconControl.propTypes = {
  icon: PropTypes.string
}
