import React, { Component } from 'react'
import Spinner from './spinner'

export default class LinearWorkflowStep extends Component {
  constructor(props) {
    super(props)

    this.state = {}

    this.setWait = this.setWait.bind(this)
    this.checkNextStep = this.checkNextStep.bind(this)
    this.checkPrevStep = this.checkPrevStep.bind(this)
    this.checkEarlyFinish = this.checkEarlyFinish.bind(this)
  }

  setWait(method) {
    this.setState({wait: true}, () => {method()})    
  }

  checkNextStep() {
    // If the beforeProgressing hook exists, we invoke it and wait for it to
    // use our callback before proceeding to the next step. This makes it
    // possible to perform work, such as validation, including asynchronous
    // work.
    if (this.props.beforeProgressing) {
      this.props.beforeProgressing((newRouteProperties) => {
        this.props.toNextStep(newRouteProperties)
      })
    }
    else {
      this.props.toNextStep()
    }

    this.setState({wait: false})
  }

  checkPrevStep() {
    // If the beforeRegresing hook exists, we invoke it and wait for it to use
    // our callback before proceeding to the previous step. This makes it
    // possible to perform work, such as validation, including asynchronous
    // work.
    if (this.props.beforeRegressing) {
      this.props.beforeRegressing((newRouteProperties) => {
        this.props.toPrevStep(newRouteProperties)
      })
    }
    else {
      this.props.toPrevStep()
    }

    this.setState({wait: false})
  }

  checkEarlyFinish() {
    // If the beforeProgressing hook exists, we invoke it and wait for it to
    // use our callback before proceeding to the next step. This makes it
    // possible to perform work, such as validation, including asynchronous
    // work.
    if (this.props.beforeProgressing) {
      this.props.beforeProgressing((newRouteProperties) => {
        this.props.finish(newRouteProperties)
      })
    }
    else {
      this.props.finish()
    }

    this.setState({wait: false})
  }

  render() {
    const PrevButton = !this.state.wait && this.props.toPrevStep &&  (
      <button onClick={this.checkPrevStep} className="prev-button">&larr; Previous</button>
    )

    const CancelButton = !this.state.wait && !this.props.toPrevStep && this.props.onCancel && ( 
      <button onClick={this.props.onCancel} className="prev-button cancel-button">Cancel</button>
    )

    const NextButton = !this.state.wait && this.props.toNextStep && (
      <button onClick={(e) => this.setWait(this.checkNextStep)} className="next-button"
              dangerouslySetInnerHTML={{__html: this.props.isFinalStep ? `Finish` : 'Next &rarr;'}}
      />
    )

    const EarlyFinishButton = !this.props.isFinalStep && this.props.showFinish && !this.state.wait && (
      <button onClick={(e) => this.setWait(this.checkEarlyFinish)} className="next-button finish-button">Finish</button>
    )

    return (
      <div className="workflow-step">
        <div className="workflow-step-content">
          {this.props.children}
        </div>

        <div className="workflow-step-controls">
          {PrevButton}
          {CancelButton}
          {EarlyFinishButton}
          {NextButton}
          {this.state.wait ? <Spinner /> : null}
          <div className="clearfix" />
        </div>
      </div>
    )
  }
}
