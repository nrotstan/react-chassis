import React, { Component } from 'react'

export default class NavMenuHeader extends Component {
  render() {
    return (
      <li className="nav-menu-header">
        <div className="header-name">{this.props.name}</div>
      </li>
    )
  }
}
