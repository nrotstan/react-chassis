import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
 
export default class Dropdown extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isExpanded: false
    }

    this.toggleExpanded = this.toggleExpanded.bind(this)
  }

  toggleExpanded(event) {
    event.preventDefault()

    this.setState({isExpanded: !this.state.isExpanded})
  }

  render() {
    const indicator = this.props.dropdownIndicator ?
                      this.props.dropdownIndicator :
                      <i className="fa fa-caret-down dropdown-indicator" />

    return (
      <div className={classNames('dropdown-menu', {expanded: this.state.isExpanded}, this.props.className)}
           onClick={this.toggleExpanded}>
        <div className="dropdown-title">
          {this.props.name} {indicator}
        </div>

        <ul className={this.state.isExpanded ? "dropdown-options active" : "dropdown-options"}>
          {this.props.children}
        </ul>
      </div>
    )
  }
}

Dropdown.propTypes = {
  name: PropTypes.string,
}
