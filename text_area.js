import React, { Component } from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
 
export default class TextArea extends Component { 
  constructor(props) {
    super(props)

    this.state = {fieldValue: this.props.fieldValue || ""}

    this.handleChange = this.handleChange.bind(this)
    this.handleOnBlur = this.handleOnBlur.bind(this)
  }

  handleChange(event) {
    this.setState({fieldValue: event.target.value})
  }

  handleOnBlur(event) {
    this.props.valueSet(this.props.name, this.state.fieldValue)

    this.setState({showRequiredWarning: (this.props.isRequired && !this.state.fieldValue)})
  }

  render() {
    const FieldLabel = this.props.label && <label>{this.props.label} {!this.props.isRequired ? null : (<span className="required-indicator">*</span>)}</label>
    const helpText = this.props.helpText ? <span className="help-text">{this.props.helpText}</span> : null
    const elementProps = _.pick(this.props, ['rows', 'columns'])

    let labelBlock = null
    if (this.props.label || this.props.helpText) {
      labelBlock = (
        <div className="label-wrapper">
          {FieldLabel}
          {helpText}
        </div>
      )
    }

    return (
      <div className="form-group text-input">
        {labelBlock}
        <textarea name={this.props.name}
                  value={this.state.fieldValue}
                  onChange={this.handleChange}
                  onBlur={this.handleOnBlur}
                  className={this.state.showRequiredWarning ? "make-required" : ""}
                  {...elementProps} />
        {this.state.showRequiredWarning ? <span className="required-warning">This field is required.</span> : null}             
      </div>
    )
  }
}

TextArea.propTypes = {
  valueSet: PropTypes.func,
  name: PropTypes.string,
}
