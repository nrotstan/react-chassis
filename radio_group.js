import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import _ from 'lodash-uuid'
 
export default class RadioGroup extends Component { 
  constructor(props) {
    super(props)
    this.state = {fieldValue: this.props.fieldValue || ""}

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    this.setState({fieldValue: event.target.value})
    this.props.valueSet(this.props.name, event.target.value)
  }

  render() {    
    const name = this.props.name + _.uuid()

    const helpText = this.props.helpText ? <span className="help-text">{this.props.helpText}</span> : null

    const radioFields = this.props.possibleValues.map(([value, label]) => (
      <div className={classNames('radio-option', {inline: this.props.inline})} key={value}>
        <input type="radio"
               name={name}
               value={value}
               checked={this.state.fieldValue == value}
               onChange={this.handleChange} />
        {label}
      </div>
    ))

    let labelBlock = null
    if (this.props.label || this.props.helpText) {
      labelBlock = (
        <div className="label-wrapper">
          <label>{this.props.label} {!this.props.isRequired ? null : (<span className="required-indicator">*</span>)}</label>
          {helpText}
        </div>
      )
    }

    return (
      <div className="form-group radio-input">
        {labelBlock}
        {radioFields}
      </div>
    )
  }
}

RadioGroup.propTypes = {
  valueSet: PropTypes.func,
  name: PropTypes.string,
  possibleValues: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
}
