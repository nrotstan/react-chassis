import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactGA from 'react-ga'
import classNames from 'classnames'
import _ from 'lodash'

const pathInterpolationRegex = new RegExp("\{([^\}]+)\}", "g")

// Supports basic routing with push-state and simple URL templating
export default class Router extends Component {
  constructor(props) {
    super(props)

    this.state = {currentRouteName: null, currentRouteProperties: {}}

    this.changeRoute = this.changeRoute.bind(this)
    this.toDefaultRoute = this.toDefaultRoute.bind(this)
    this.updateRouteProperties = this.updateRouteProperties.bind(this)
  }

  componentDidMount() {
    window.onpopstate = (event) => {
      this.loadFromBrowserState(event.state)
    }

    // When the page first loads, there could be a state but we won't get an
    // onpopstate event for initial load. Check the history.state to see if
    // there is any existing state that matches a route. If not, try to match
    // the URL to a route. If that doesn't work either, then use replaceState
    // for the default route to ensure that the browser URL matches up with the
    // page that will be rendered.
    if (this.routeMatchingState(window.history.state)) {
      this.loadFromBrowserState(window.history.state)
    }
    else {
      let matchingRoute = this.routeMatchingBrowserURL()
      if (matchingRoute) {
        const routeState = {
          currentRouteName: matchingRoute.name,
          currentRouteProperties: {}
        }

        this.loadFromBrowserState(routeState)
      }
      else {
        const route = this.routeWithName(this.props.defaultRouteName)
        window.history.replaceState({}, null, this.normalizedRoutePath(route))
      }
    }
  }

  // Load route from onpopstate event state or history.state
  loadFromBrowserState(browserState) {
    if (_.isObject(browserState) &&
        _.isString(browserState.currentRouteName) &&
        _.isObject(browserState.currentRouteProperties)) {

      this.populateRoutePropertiesFromBrowserURL(browserState)
      this.changeRoute(browserState.currentRouteName,
                       browserState.currentRouteProperties)
    }
  }

  changeRoute(newRouteName, newRouteProperties = {}) {
    const newRouteState = {
      currentRouteName: newRouteName,
      currentRouteProperties: newRouteProperties
    }

    if (this.props.clearNotification) {
      this.props.clearNotification()
    }

    this.setState(newRouteState)

    const newUrl = this.interpolatedBrowserPath(newRouteState)
    window.history.pushState(newRouteState, null, newUrl)
    window.scrollTo(0, 0)

    ReactGA.set({page: newUrl})
    ReactGA.pageview(newUrl)
  }

  toDefaultRoute() {
    this.changeRoute(this.props.defaultRouteName)
  }

  updateRouteProperties(updatedProperties, scrollToTop=false) {
    const existingProperties = _.cloneDeep(this.state.currentRouteProperties)
    const newRouteProperties = {currentRouteProperties: _.merge(existingProperties, updatedProperties)}

    this.setState(newRouteProperties)
    window.history.replaceState(_.merge({}, this.state, newRouteProperties),
                                null,
                                this.state.currentRouteName)
    if (scrollToTop) {
      window.scrollTo(0, 0)
    }
  }

  interpolatedBrowserPath(routeState) {
    const route = this.routeWithName(routeState.currentRouteName)

    if (route) {
      const path = this.normalizedRoutePath(route)

      // If the path contains interpolation variables, substitute them with values.
      if (path.indexOf('{') > -1) {
        let interpolatedPath = path
        let match = null

        while ((match = pathInterpolationRegex.exec(path)) !== null) {
          interpolatedPath = interpolatedPath.replace(
            match[0],
            _.get(routeState.currentRouteProperties, match[1], '')
          )
        }

        return interpolatedPath
      }
      else {
        return path
      }
    }
    else {
      return routeState.currentRouteName
    }
  }

  populateRoutePropertiesFromBrowserURL(routeState) {
    const route = this.routeWithName(routeState.currentRouteName)

    // Only continue if the route has a path utilizing interpolation variables
    if (route && route.path && route.path.indexOf('{') > -1) {
      const routeTokens = route.path.split('/')
      const urlTokens = window.location.pathname.split('/')

      if (routeTokens.length == urlTokens.length) {
        for (let i = 0; i < routeTokens.length; i++) {
          if (routeTokens[i].charAt(0) == '{') {
            // strip out { and } characters from variable
            const variableName = routeTokens[i].substring(1, routeTokens[i].length - 1)

            // Don't overwrite a proper variable passed through by the app
            if (!routeState.currentRouteProperties[variableName]) {
              routeState.currentRouteProperties[variableName] = urlTokens[i]
            }
          }
        }
      }
    }
  }

  routeMatchingBrowserURL() {
    const urlTokens = window.location.pathname.split('/')

    if (urlTokens.length > 1) {
      for (let route of this.props.routes) {
        const path = this.normalizedRoutePath(route)
        if (this.routeMatchesURL(path.split('/'), urlTokens)) {
          return route
        }
      }
    }

    return null
  }

  routeMatchingState(routeState) {
    if (_.isObject(routeState) && _.isString(routeState.currentRouteName)) {
      return this.routeWithName(routeState.currentRouteName)
    }
    else {
      return null
    }
  }

  routeMatchesURL(routeTokens, urlTokens) {
    if (routeTokens.length != urlTokens.length) {
      return false
    }

    for (let i = 0; i < routeTokens.length; i++) {
      // unless the route has an interpolation variable, make sure the token
      // matches.
      if (routeTokens[i].charAt(0) != '{' && routeTokens[i] !== urlTokens[i]) {
        return false
      }
    }

    return true
  }

  routeWithName(name) {
    return _.find(this.props.routes, {name})
  }

  normalizedRoutePath(route) {
    return route.path ? route.path : `/${_.kebabCase(route.name)}`
  }

  render() {
    const routeName = this.state.currentRouteName || this.props.defaultRouteName
    const route = this.routeWithName(routeName)
    const RouteComponent = route ? route.component : this.props.route404

    const routeProxy = {
      name: routeName,
      changeTo: this.changeRoute,
      changeToDefault: this.toDefaultRoute,
      reloadWith: this.updateRouteProperties,
    }

    const fullScreen = _.get(this.state, 'currentRouteProperties.fullScreen')
    return (
      <div className={classNames({"full-screen": fullScreen})}>
        {!fullScreen && this.props.header && React.cloneElement(this.props.header, {route: routeProxy})}
        {this.props.children && React.cloneElement(this.props.children, {route: routeProxy})}
        <RouteComponent route={routeProxy}
                        {...this.state.currentRouteProperties}
                        {...(_.omit(this.props, ['routes', 'defaultRouteName', 'route404']))} />
        {!fullScreen && this.props.footer && React.cloneElement(this.props.footer, {route: routeProxy})}
      </div>
    )
  }
}

Router.propTypes = {
  routes: PropTypes.arrayOf(PropTypes.object),
  defaultRouteName: PropTypes.string
}

Router.defaultProps = {
  route404: function() { return <h1>404: Page Not Found</h1> }
}
