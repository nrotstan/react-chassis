import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class CollapsibleSection extends Component {
  constructor(props) {
    super(props)

    this.state = { isExpanded: this.props.initiallyExpanded }
    this.toggleExpansion = this.toggleExpansion.bind(this)
  }

  toggleExpansion() {
    this.setState({isExpanded: !this.state.isExpanded})
  }

  render() {
    const toggleControl =
      this.state.isExpanded ?
      (<i className="toggle-control fa fa-minus-square-o"></i>) :
      (<i className="toggle-control fa fa-plus-square-o"></i>)

    const children = this.state.isExpanded ? this.props.children : null

    return (
      <div className="collapsible-section">
        <h5 className="header" onClick={this.toggleExpansion}>
          {toggleControl} {this.props.header}
        </h5>

        {children}
      </div>
    )
  }
}

CollapsibleSection.propTypes = {
  header: PropTypes.string,
  initiallyExpanded: PropTypes.bool,
}

CollapsibleSection.defaultProps = {
  initiallyExpanded: false
}
