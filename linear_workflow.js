import React, { Component } from 'react'
import _ from 'lodash'

// Represents a series of steps that can be navigated
// forwards or backwards in a linear fashion from step
// to step.
export default class LinearWorkflow extends Component {
  constructor(props) {
    super(props)

    this.alterStep = this.alterStep.bind(this);
    this.toPrevStep = this.toPrevStep.bind(this);
    this.toNextStep = this.toNextStep.bind(this);
  }

  alterStep(stepNumber, newRouteProperties = {}) {
    if (stepNumber >= 0 && stepNumber < this.props.workflowSteps.length) {
      this.props.route.reloadWith(_.merge(newRouteProperties, {workflowStep: stepNumber}),
                                  true)
    }
  }

  toPrevStep(newRouteProperties = {}) {
    const stepNumber = _.isNumber(this.props.workflowStep) ? this.props.workflowStep : 0
    this.alterStep(stepNumber - 1, newRouteProperties)
  }

  toNextStep(newRouteProperties = {}) {
    const stepNumber = _.isNumber(this.props.workflowStep) ? this.props.workflowStep : 0
    this.alterStep(stepNumber + 1, newRouteProperties)
  }

  render() {
    const stepNumber = _.isNumber(this.props.workflowStep) ? this.props.workflowStep : 0
    const CurrentStep = this.props.workflowSteps[stepNumber]
    const isFinalStep = stepNumber == this.props.workflowSteps.length - 1

    return (
      <div className="linear-workflow">
        <CurrentStep key={this.props.workflowStep}
                     toNextStep={isFinalStep ? this.props.finish : this.toNextStep}
                     toPrevStep = {this.props.workflowStep > 0 ? this.toPrevStep : null}
                     isFinalStep={isFinalStep}
                     {...this.props} />
      </div>
    )
  }
}
