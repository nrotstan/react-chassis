import React, { Component } from 'react'

export default class NavMenuOption extends Component {
  constructor(props) {
    super(props)

    this.chooseOption = this.chooseOption.bind(this)
    this.proceed = this.proceed.bind(this)
  }

  chooseOption() {
    if (this.props.onChoose) {
      this.props.onChoose()
    }
    else {
      this.props.beforeRouting ? this.props.beforeRouting(this.proceed) : this.proceed()
    }
  }

  proceed() {
    this.props.route.changeTo(this.props.destination, this.props.destinationProps)
  }

  render() {
    return (
      <li className="nav-menu-link clickable" onClick={this.chooseOption}>
        <div className="option-name">{this.props.name}</div>
      </li>
    )
  }
}

