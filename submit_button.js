import React, { Component } from 'react'
import PropTypes from 'prop-types'
 
export default class SubmitButton extends Component { 
  render() {
    const label = this.props.label ? this.props.label : this.props.children || 'Submit'

    return (
      <button className="submit-button"
              onClick={this.props.submitForm}>{label}</button>
    )
  }
}

SubmitButton.propTypes = {
  submitForm: PropTypes.func.isRequired,
  label: PropTypes.string
}
