import React, { Component } from 'react'
import _ from 'lodash'

import RouteLink from './route_link'

export default class BreadcrumbNavigation extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    if (!_.get(this.props, "breadcrumbs.length") > 0) {
      return null
    }

    const navs = _.map(this.props.breadcrumbs, (breadcrumb, index) => {
      let navItem = null
      if (index < this.props.breadcrumbs.length - 1 && breadcrumb.routeName) {
        navItem = <RouteLink {...this.props}
                             name={breadcrumb.label}
                             destination={breadcrumb.routeName}
                             destinationProps={breadcrumb.routeProps} />
      }
      else {
        navItem = breadcrumb.label
      }

      return <li key={breadcrumb.label} className="breadcrumb-nav-item">{navItem}</li>
    })

    return <ul className="breadcrumb-navigation">{navs}</ul>
  }
}
