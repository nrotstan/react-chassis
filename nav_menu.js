import React, { Component } from 'react'
import PropTypes from 'prop-types'
 
export default class NavMenu extends Component {
  constructor(props) {
    super(props)

    this.toggleMenu = this.toggleMenu.bind(this)
  }

  toggleMenu(event) {
    event.preventDefault()

    this.props.selectMenu(this.isActive() ? null : this.props.menuId)
  }

  isActive() {
    return this.props.activeMenu == this.props.menuId
  }

  render() {
    const indicator = this.props.dropdownIndicator ?
                      this.props.dropdownIndicator :
                      <i className="fa fa-caret-down dropdown-indicator" />

    return (
      <li className="nav-control nav-menu" id={this.props.menuId} key={this.props.menuId}>
        <a href="#" className="nav-menu-control" onClick={this.toggleMenu}>
          <span className="nav-menu-icon">{this.props.icon}</span>
          <span className="control-name">{this.props.name}</span> {indicator}
        </a>

        <ul className={this.isActive() ? "nav-menu-body active" : "nav-menu-body"}>
          {this.props.children}
        </ul>
      </li>
    )
  }
}

NavMenu.propTypes = {
  menuId: PropTypes.string.isRequired,
}
