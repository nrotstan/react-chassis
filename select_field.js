import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Select from 'react-select';
import classNames from 'classnames'

export default class SelectField extends Component {
  constructor(props) {
    super(props)

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(newValue) {
    if (this.props.valueSet) {
      this.props.valueSet(this.props.name, newValue)
    }
  }

  render() {
    const FieldLabel = this.props.label && <label>{this.props.label} {!this.props.isRequired ? null : (<span className="required-indicator">*</span>)}</label>
    const helpText = this.props.helpText ? <span className="help-text">{this.props.helpText}</span> : null
    const leader = this.props.leader && <span className="leader">{this.props.leader}</span>
    const trailer = this.props.trailer && <span className="trailer">{this.props.trailer}</span>
    const hasAddons = !!(leader || trailer)
    let selectField = null

    if (this.props.creatable) {
      selectField =
        <Select.Creatable name={this.props.name}
              placeholder={this.props.placeholder}
              promptTextCreator={this.props.createPlaceholder}
              multi={this.props.multi || false}
              clearable={this.props.clearable !== false}
              value={this.props.selectedItems || this.props.fieldValue}
              options={this.props.items}
              onNewOptionClick={this.props.newOption}
              onInputChange={this.props.valueChanged}
              onChange={this.handleChange} />
    }
    else {
      selectField =
        <Select name={this.props.name}
            placeholder={this.props.placeholder}
            multi={this.props.multi || false}
            clearable={this.props.clearable !== false}
            value={this.props.selectedItems || this.props.fieldValue}
            options={this.props.items}
            onInputChange={this.props.valueChanged}
            onChange={this.handleChange} />
    }

    let labelBlock = null
    if (this.props.label || this.props.helpText) {
      labelBlock = (
        <div className="label-wrapper">
          {FieldLabel}
          {helpText}
        </div>
      )
    }

    return (
      <div className={classNames('form-group', 'select-input', this.props.className,
                                 {'with-addons': hasAddons})}>
        {labelBlock}
        <div className={classNames('input-wrapper',
                                   {"with-leader": !!leader, "with-trailer": !!trailer})}>
          {leader}
          {selectField}
          {trailer}
        </div>
      </div>
    )
  }
}

SelectField.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.string,
  })).isRequired,
  selectedItems: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.string,
  })),
  fieldValue: PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.string,
  }),
  valueSet: PropTypes.func.isRequired,
  valueChanged: PropTypes.func,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  createPlaceholder: PropTypes.func,
  multi: PropTypes.bool,
  clearable: PropTypes.bool,
}
