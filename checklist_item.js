import React, { Component } from 'react'
 
export default class ChecklistItem extends Component { 
  constructor(props) {
    super(props)

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    event.target.checked ? this.props.checkItem(this.props.item) :
                           this.props.uncheckItem(this.props.item)
  }

  render() {
    return (
      <div className="checklist-item">
        <label>
          <input type="checkbox"
                 checked={this.props.value === true}
                 onChange={this.handleChange} />
          <span className="name">{this.props.item.name}</span>
        </label>
      </div>
    )
  }
}
