import React, { Component } from 'react'
import classNames from 'classnames'
import _ from 'lodash'

export default class Tabs extends Component {
  constructor(props) {
    super(props)

    this.state = {
      activeTab: null
    }

    this.selectTab = this.selectTab.bind(this)
  }

  selectTab(tabId) {
    this.setState({activeTab: tabId})
  }

  render() {
    // If no active tab, default to first tab.
    const activeTab = this.state.activeTab ||
                      this.props.defaultTab ||
                      this.props.tabLabels[0].props.tabId

    let activePane = _.find(this.props.tabPanes, (pane) => pane.props.tabId === activeTab)

    // create clones of labels to deliver updated props
    const labels = this.props.tabLabels.map((label) =>
      React.cloneElement(label, {activeTab: activeTab, selectTab: this.selectTab})
    )

    return (
      <div className="tab-set">
        <ul className="tab-bar">{labels}</ul>
        {activePane}
      </div>
    )
  }
}
