import React, { Component } from 'react'
import PropTypes from 'prop-types'
 
export default class RouteLink extends Component {
  constructor(props) {
    super(props)

    this.activate = this.activate.bind(this)
  }

  activate(event) {
    event.preventDefault()
    let proceed = null

    if (this.props.defaultDestination || !this.props.destination) {
      proceed = () => this.props.route.changeToDefault()
    }
    else {
      proceed = () => this.props.route.changeTo(this.props.destination, this.props.destinationProps)
    }

    this.props.beforeRouting ? this.props.beforeRouting(proceed) : proceed()
  }

  render() {
    const label = this.props.name ? this.props.name : this.props.children

    return this.props.asButton ?
           <button className={this.props.linkClasses} onClick={this.activate}>{label}</button> :
           <a href="#" className={this.props.linkClasses} onClick={this.activate}>{label}</a>
  }
}

RouteLink.propTypes = {
  asButton: PropTypes.bool,
  name: PropTypes.string,
  destination: PropTypes.string,
  defaultDestination: PropTypes.bool,
  destinationProps: PropTypes.object,
  route: PropTypes.object.isRequired,
}

RouteLink.defaultProps = {
  asButton: false,
  defaultDestination: false,
  destinationProps: {},
}
