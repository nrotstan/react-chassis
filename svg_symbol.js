import React from 'react'

export default function SvgSymbol(props) {
  return (
    <svg viewBox={props.viewBox} className={props.className} onClick={props.onClick}>
      <use xlinkHref={'#' + props.sym}></use>
    </svg>
  )
}
