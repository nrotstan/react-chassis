import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

export default class InputField extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fieldValue: (typeof this.props.fieldValue === 'undefined' || this.props.fieldValue === null) ? "" :
                  this.props.fieldValue,
      focused: false,
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleOnFocus = this.handleOnFocus.bind(this)
    this.handleOnBlur = this.handleOnBlur.bind(this)
    this.checkForEnter = this.checkForEnter.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (typeof nextProps.fieldValue !== 'undefined' &&
        nextProps.fieldValue !== this.props.fieldValue) {
      this.setState({fieldValue: nextProps.fieldValue})
    }
  }

  handleChange(event) {
    this.setState({fieldValue: event.target.value})

    if (this.props.valueChanged) {
      this.props.valueChanged(event.target.value)
    }
  }

  handleOnFocus(event) {
    this.setState({focused: true})
  }

  handleOnBlur(event) {
    if (this.props.valueSet) {
      this.props.valueSet(this.props.name, this.state.fieldValue)
    }

    this.setState({
      focused: false,
      showRequiredWarning: (this.props.isRequired &&
                            (typeof this.state.fieldValue === 'undefined' || this.state.fieldValue === null))
    })
  }

  checkForEnter(event) {
    if (event.key === 'Enter'){
      this.handleOnBlur(event)

      if (this.props.onEnterKey) {
        this.props.onEnterKey()
      }
    }
  }

  render() {
    const FieldLabel = this.props.label && <label>{this.props.label} {!this.props.isRequired ? null : (<span className="required-indicator">*</span>)}</label>
    const helpText = this.props.helpText ? <span className="help-text">{this.props.helpText}</span> : null
    const leader = this.props.leader && <span className="leader">{this.props.leader}</span>
    const trailer = this.props.trailer && <span className="trailer">{this.props.trailer}</span>
    const hasAddons = !!(leader || trailer)

    let labelBlock = null
    if (this.props.label || this.props.helpText) {
      labelBlock = (
        <div className="label-wrapper">
          {FieldLabel}
          {helpText}
        </div>
      )
    }

    return (
      <div className={classNames('form-group', 'text-input', this.props.className,
                                 {'with-addons': hasAddons})}>
        {labelBlock}
        <div className={classNames('input-wrapper', {"with-focus": this.state.focused},
                                   {"with-leader": !!leader, "with-trailer": !!trailer})}>
          {leader}
          <input type={this.props.type || "text"}
                min={this.props.min}
                name={this.props.name}
                value={this.state.fieldValue}
                placeholder={this.props.placeholder}
                onKeyPress={this.props.onEnterKey ? this.checkForEnter : undefined}
                onChange={this.handleChange}
                disabled={this.props.disabled == true ? "disabled" : ""}
                onFocus={this.handleOnFocus}
                onBlur={this.handleOnBlur}
                className={this.state.showRequiredWarning ? "make-required" : ""}/>
          {this.state.showRequiredWarning ? <span className="required-warning">This field is required.</span> : null}
          {trailer}
        </div>
      </div>
    )
  }
}

InputField.propTypes = {
  valueSet: PropTypes.func,
  name: PropTypes.string
}
