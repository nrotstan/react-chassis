import React, { Component } from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'

import ChecklistItem from './checklist_item'
 
export default class Checklist extends Component { 
  constructor(props) {
    super(props)
  }

  render() {
    const checklistItems = this.props.items.map((item) =>
      <ChecklistItem key={item._id}
                     item={item}
                     checkItem={this.props.checkItem}
                     uncheckItem={this.props.uncheckItem}
                     value={this.props.checkedItems.indexOf(item._id) > -1} />
    )

    return (
      <div className="checklist">{checklistItems}</div>
    )
  }
}

Checklist.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
  })).isRequired,
  checkedItems: PropTypes.arrayOf(PropTypes.string),

  checkItem: PropTypes.func,
  uncheckItem: PropTypes.func
}

Checklist.defaultProps = {
  checkedItems: []
}
