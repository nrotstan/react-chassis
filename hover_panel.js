import React, { Component } from 'react'

import IconControl from './icon_control'

// A panel with an activation control that is displayed either when
// the mouse hovers over the activation control or when the activation
// control is clicked. Whichever method is used to activate the panel
// (hover or click) must also be used to cancel display of the panel
// in order to ensure a consistent user experience. Making use of a
// panel control always deactivates the panel, regardless of how the
// panel was activated.
export default class HoverPanel extends Component {
  constructor(props) {
    super(props)

    this.state = {
      showPanel: false,
      activatedByHover: false,
    }

    this.activate = this.activate.bind(this)
    this.deactivate = this.deactivate.bind(this)
    this.hoverDeactivate = this.hoverDeactivate.bind(this)
    this.toggleClickActivation = this.toggleClickActivation.bind(this)
  }

  activate(fromHover = false) {
    this.setState({showPanel: true, activatedByHover: fromHover})
  }

  hoverDeactivate() {
    // Ignore hover deactivation if the panel was shown due to clicking.
    if (this.state.showPanel && this.state.activatedByHover) {
      this.deactivate()
    }
  }

  deactivate() {
    this.setState({showPanel: false, activatedByHover: false})
  }

  toggleClickActivation(event) {
    event.stopPropagation()

    // Ignore click deactivation if the panel was shown due to hovering.
    if (!this.state.showPanel || !this.state.activatedByHover) {
      this.setState({showPanel: !this.state.showPanel, activatedByHover: false})
    }
  }

  render() {
    const classNames = this.props.className ? `hover-panel ${this.props.className}` : 'hover-panel'
    const children = this.state.showPanel ? this.props.children : null

    return (
      <div className={classNames}>
        <div className='controls' onClick={this.deactivate} onMouseLeave={this.hoverDeactivate}>
          <IconControl icon="fa fa-fw fa-gear"
                       onClick={this.toggleClickActivation}
                       onMouseEnter={this.activate}
                       title={this.state.showPanel ? 'Hide controls' : 'Show controls'} />
          {children}
        </div>
      </div>
    )
  }
}
