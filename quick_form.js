import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import _ from 'lodash'

import InputField from './input_field'
import TextArea from './text_area'
import SubmitButton from './submit_button'

export default class QuickForm extends Component { 
  constructor(props) {
    super(props)

    this.state = {
      fields: {}
    }

    this.fieldSet = this.fieldSet.bind(this)
    this.submitForm = this.submitForm.bind(this)
  }

  fieldSet(key, value) {
    const fields = this.state.fields
    _.set(fields, key, value)

    this.setState({fields})
  }

  submitForm() {
    this.props.objectSet(this.props.name, this.state.fields)
  }

  inputFor(field) {
    switch(field.type) {
      case 'textarea':
        return <TextArea name={field.name}
                         key={field.name}
                         label={field.label}
                         value={_.get(this.state.fields, field.name)}
                         valueSet={this.fieldSet} />
        break
      default:
        return <InputField name={field.name}
                           key={field.name}
                           label={field.label}
                           type={field.type}
                           placeholder={field.placeholder}
                           value={_.get(this.state.fields, field.name)}
                           valueSet={this.fieldSet}
                           onEnterKey={this.props.allowKeyboardSubmit ? this.submitForm : undefined} />
    }
  }

  render() {
    let fieldName = null
    let fieldType = null

    const inputs = this.props.fields.map((field) => this.inputFor(field))
    
    let submitButton = null
    if (this.props.submitControl) {
      submitButton = React.cloneElement(this.props.submitControl, {onClick: this.submitForm})
    }
    else {
      const submitLabel = this.props.submitLabel || 'Submit'
      submitButton =
        <SubmitButton label={submitLabel} submitForm={this.submitForm} />
    }

    return (
      <div className={classNames('form', this.props.className)}>
        {inputs}
        {submitButton}
      </div>
    )
  }
}

QuickForm.propTypes = {
  name: PropTypes.string.isRequired,
  fields: PropTypes.array.isRequired,
  objectSet: PropTypes.func.isRequired,
}
